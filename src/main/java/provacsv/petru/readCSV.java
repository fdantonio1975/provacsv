package provacsv.petru;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.IOException;
import java.io.InputStream;
import java.net.URISyntaxException;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Properties;

import org.apache.commons.csv.CSVFormat;
import org.apache.commons.csv.CSVParser;
import org.apache.commons.csv.CSVRecord;
import com.google.common.io.Resources;
import com.hp.hpl.jena.ontology.OntClass;
import com.hp.hpl.jena.ontology.OntModel;
import com.hp.hpl.jena.ontology.OntModelSpec;
import com.hp.hpl.jena.rdf.model.Model;
import com.hp.hpl.jena.rdf.model.ModelFactory;
import com.hp.hpl.jena.rdf.model.Property;
import com.hp.hpl.jena.rdf.model.Resource;
import com.hp.hpl.jena.util.FileManager;
import com.hp.hpl.jena.util.iterator.ExtendedIterator;
import java.util.Map.Entry;

public class readCSV {

	public static void main(String args[]) throws IOException, URISyntaxException {

		// Load TBOX
		Properties prop = new Properties();
		String inputFileName = "/home/pbzulan/Downloads/tbox2.owl";
		// InputStream input = null;

		// Prefix Settings
		Model model = ModelFactory.createDefaultModel();
		String prefix = "http://rdf.aubay.it/opendata/infortuni/";
		model.setNsPrefix("", prefix);
		Properties mapping = new Properties();
		mapping.load(Resources.getResource("mapping.properties").openStream());
		CSVFormat format = CSVFormat.EXCEL.withDelimiter(';').withHeader();
		CSVParser parse = format.parse(
				new FileReader(new File(Resources.getResource("DatiConCadenzaMensileInfortuniMolise.csv").toURI())));
		Iterator<CSVRecord> list = parse.iterator();
		while (list.hasNext()) {
			CSVRecord r = (CSVRecord) list.next();
			Map<String, String> map = r.toMap();
			for (Entry<String, String> e : map.entrySet()) {

				if (mapping.getProperty(e.getKey()) == null) {

				} else {
					System.out.println(mapping.getProperty(e.getKey()) + ":" + e.getValue() + "\n");
				}
			}

		}
	}
}