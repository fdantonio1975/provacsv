package provacsv.petru;


import java.io.File;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.IOException;
import java.net.URISyntaxException;
import java.util.Iterator;
import java.util.Map;
import java.util.Properties;
import java.util.Map.Entry;
import org.apache.commons.csv.CSVFormat;
import org.apache.commons.csv.CSVParser;
import org.apache.commons.csv.CSVRecord;
import com.google.common.io.Resources;
import com.hp.hpl.jena.ontology.OntClass;
import com.hp.hpl.jena.ontology.OntModel;
import com.hp.hpl.jena.ontology.OntModelSpec;
import com.hp.hpl.jena.rdf.model.Model;
import com.hp.hpl.jena.rdf.model.ModelFactory;
import com.hp.hpl.jena.rdf.model.Property;
import com.hp.hpl.jena.rdf.model.Resource;

public class GenerateRDFfromCSV {

	public static void main(String args[]) throws IOException, URISyntaxException {

		// Load Properties and TBOX
		Properties mapping = new Properties();
		mapping.load(Resources.getResource("mapping.properties").openStream());
		OntModel inf = ModelFactory.createOntologyModel(OntModelSpec.OWL_MEM);
		inf.read(Resources.getResource("tbox.owl").openStream(), "UTF-8");

		// Ontology Settings
		String prefix = "http://rdf.aubay.it/opendata/infortuni/";
		String rdf = "http://www.w3.org/1999/02/22-rdf-syntax-ns#";

		// Load Ontology Model
		Model model = ModelFactory.createDefaultModel();
		model.setNsPrefix("", prefix);

		// Ontology Prefixes
		Property type = model.createProperty(rdf + "type");

		// CSV Settings
		CSVFormat format = CSVFormat.EXCEL.withDelimiter(';').withHeader();
		CSVParser parse = format.parse(
				new FileReader(new File(Resources.getResource("DatiConCadenzaMensileInfortuniMolise.csv").toURI())));
		Iterator<CSVRecord> list = parse.iterator();

		// Ontology Classes
		OntClass persona = inf.getOntClass(prefix + "Persona");
		OntClass polizza = inf.getOntClass(prefix + "Polizza");
		OntClass bene = inf.getOntClass(prefix + "Bene");
		OntClass luogo = inf.getOntClass(prefix + "Luogo");
		OntClass danno = inf.getOntClass(prefix + "Danno");
		OntClass societa = inf.getOntClass(prefix + "Societa");
		OntClass stato = inf.getOntClass(prefix + "Stato");

		// Property Persona
		Property hasNome = model.getProperty(prefix + "hasNome");
		Property hasSesso = model.getProperty(prefix + "hasSesso");
		Property hasEta = model.getProperty(prefix + "hasEta");
		Property hasResidenza = model.getProperty(prefix + "hasResidenza");

		// Property Polizza
		Property hasCodiceIdentificazione = model.getProperty(prefix + "hasCodiceIdentificazione");
		Property hasAssicurato = model.getProperty(prefix + "hasAssicurato");
		Property hasBeneficiario = model.getProperty(prefix + "hasBeneficiario");
		Property hasContraente = model.getProperty(prefix + "hasContraente");

		// Generate Model RDF
		while (list.hasNext()) {
			CSVRecord r = (CSVRecord) list.next();
			Map<String, String> map = r.toMap();
			Resource personaResource = model.createResource(prefix + "persona_" + r.get("IdentificativoInfortunato"));
			Resource polizzaResource = model.createResource(prefix + "polizza_" + r.get("IdentificativoCaso"));

			model.add(personaResource, type, persona);
			model.add(polizzaResource, type, polizza);
			for (Entry<String, String> e : map.entrySet()) {

				if (mapping.getProperty(e.getKey()) == null) {
				} else {
					model.add(personaResource, inf.getProperty(prefix + mapping.getProperty(e.getKey())), e.getValue());
					model.add(polizzaResource, inf.getProperty(prefix + mapping.getProperty(e.getKey())), e.getValue());

				}
			}
		}
		System.out.println(model.write(System.out));

		// Scrivi su file in RDF
		// model.write(new FileOutputStream(new
		// File("/home/pbzulan/Desktop/myRDFFile.owl")));
	}
}