package provacsv.apps;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.net.URISyntaxException;
import java.util.Iterator;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Properties;

import org.apache.commons.csv.CSVFormat;
import org.apache.commons.csv.CSVParser;
import org.apache.commons.csv.CSVRecord;

import com.google.common.io.Resources;
import com.hp.hpl.jena.ontology.OntClass;
import com.hp.hpl.jena.ontology.OntModel;
import com.hp.hpl.jena.ontology.OntModelSpec;
import com.hp.hpl.jena.rdf.model.Model;
import com.hp.hpl.jena.rdf.model.ModelFactory;
import com.hp.hpl.jena.rdf.model.Property;
import com.hp.hpl.jena.rdf.model.Resource;

public class CSVMapping {
	
	@SuppressWarnings("resource")
	public static void main(String[] args) throws FileNotFoundException, IOException, URISyntaxException {
		Properties mapping = new Properties();
		mapping.load(Resources.getResource("mapping.properties").openStream());
		
		
		OntModel inf = ModelFactory.createOntologyModel(OntModelSpec.OWL_MEM);
		inf.read(Resources.getResource("tbox3.owl").openStream(),"UTF-8");
		
		CSVFormat format = CSVFormat.EXCEL.withDelimiter(';').withHeader();
		CSVParser parse = format.parse(new FileReader(new File(Resources.getResource("data/prova.csv").toURI())));
		Iterator<CSVRecord> list = parse.iterator();
		Model model = ModelFactory.createDefaultModel();
		String prefix = "http://rdf.aubay.it/opendata/infortuni/";
		model.setNsPrefix("", prefix);
		String rdf="http://www.w3.org/1999/02/22-rdf-syntax-ns#";
		Property type = model.createProperty(rdf+"type");
		OntClass persona = inf.getOntClass(prefix+"Persona");

		while (list.hasNext()) {
			CSVRecord r = (CSVRecord) list.next();
			Map<String, String> map = r.toMap();
			Resource personaResource = model.createResource(prefix+"persona_"+r.getRecordNumber());
			model.add(personaResource,type,persona);
			for (Entry<String, String> e : map.entrySet()) {
				model.add(personaResource,inf.getProperty(prefix+mapping.getProperty(e.getKey())),e.getValue());
			}
		}
		System.out.println(model.write(System.out));

	}

}
